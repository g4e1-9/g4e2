//GUIA MANEJO STRINGS EJERCICIO 2
//Permitir el ingreso de una palabra y mostrarla en pantalla al revés.
//Por ejemplo, para "CASA" se debe mostrar "ASAC".

#include<stdio.h>

int main ()

{
    char palabra [20]; //Declaro las variables.
    int i = 0;
    
    printf ("Ingrese una palabra: "); //Solicito el ingreso de una palabra.
    scanf ("%s", palabra);
    
    while (palabra[i++]!= '\0'); //Hago el conteo de letras.
    
    printf("La palabra escrita a la inversa es: ", palabra);
    
    while (i>0) //Invierto la palabra.
    
    printf("%c", palabra[--i]); //Muestro la palabra al revés.
    
    return 0;
}